import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oja on 18.6.2016.
 */
public class ProjectCompile extends ProjectTask {

    public ProjectCompile() {
        super();
        configFile = readConfigFile();
    }

    /**
     * Method from the parent class. It parses the input parameters, checks the requirements for this task.
     * If everything is fine then the 'Compile project' method gets called.
     *
     * @param args String array value of the input parameters.
     */
    void parse(String[] args) {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);

            if (checkRequirements(cmd)) {
                executeConfigFileScript(true, "compile");
                compileProject();
            } else {
                System.exit(0);
            }
        } catch (ParseException ex) {
            System.err.println("Failed to parse command line properties:" + ex);
            System.exit(0);
        }
    }

    /**
     * Method used to do the compile process.
     */
    private void compileProject() {
        createObjFolder();
        createRFile();
        compileClasses();
        executeConfigFileScript(false, "compile");
    }

    /**
     * Method that is used to create the R file using the command line.
     */
    private void createRFile() {
        System.out.println("Creating R file...");
        String aaptLocation = Paths.get(BuildUtils.ANDROID_HOME, "build-tools", (String) configFile.get("buildToolsVersion"), "aapt").toString();
        String androidLocation = Paths.get(BuildUtils.ANDROID_HOME, "platforms", (String) configFile.get("targetSDK"), "android.jar").toString();
        String command = BuildUtils.extraCommand + aaptLocation + " package -v -f -m -S res -J src -M AndroidManifest.xml -I " + androidLocation;

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("R file created successfully");
            } else {
                System.out.println("Failed to create R file");
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String error;
                while ((error = stdInput.readLine()) != null) {
                    System.err.println(error);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Failed to create R file");
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method used to create an Obj folder where the compiled classes will be stored.
     */
    private void createObjFolder() {
        System.out.println("Creating obj folder...");
        Path path = Paths.get("obj");
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
                System.out.println("Obj folder created.");
            } catch (IOException e) {
                System.out.println("Failed to create obj folder");
                System.err.println(e);
            }
        }
    }

    /**
     * Method used to compile all the classes. It prepares the data, calls  the getLibs method that gets the 3-rd party
     * libraries. Calls the compilePackages method which is a recursive method that finds all the packages and compiles
     * them separately.
     */
    private void compileClasses() {
        System.out.println("Compiling classes...");

        String packageName = (String) configFile.get("packageName");
        String[] packageArray = packageName.split("\\.");
        String srcPath = "src";
        for (String packageString : packageArray) {
            srcPath = Paths.get(srcPath, packageString).toString();
        }
        compilePackages(srcPath, getLibs());
    }

    /**
     * Recursive method that goes through all the folders in the package. And compiles each folder containing a java class.
     *
     * @param path String value of the current path in where the checks are being made.
     * @param libs String value of the list of 3-rd party libraries
     */
    private void compilePackages(String path, String libs) {
        File folder = new File(path);
        FilenameFilter jarFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return (name.toLowerCase().endsWith(".java") || dir.isDirectory());
            }
        };
        File[] listOfFiles = folder.listFiles(jarFilter);
        for (File file : listOfFiles) {
            if (file.isFile()) {
                String finalPath = Paths.get(path, "java").toString();
                compile(new StringBuilder(finalPath).insert(finalPath.length() - 4, "*.").toString(), libs);
                break;
            } else if (file.isDirectory()) {
                compilePackages(Paths.get(path, file.getName()).toString(), libs);
            }
        }
    }

    /**
     * Method that compiles a package with command line.
     *
     * @param path String value of the location of the package
     * @param libs String value of the list of libraries.
     */
    private void compile(String path, String libs) {
        String command = BuildUtils.extraCommand + "javac -verbose -d obj -classpath " + libs + " -sourcepath src " + path;
        System.out.println(command);
        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader output = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line;
            while ((line = output.readLine()) != null) {
                System.out.println(line);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("Package with path " + path + " compiled successfully");
            } else {
                System.out.println("Failed to compile package with path " + path);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String error;
                while ((error = stdInput.readLine()) != null) {
                    System.err.println(error);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method used to get all the 3-rd party libraries including the android jar in a list that will be used to compile
     * project.
     *
     * @return String value of the list of all the libraries.
     */
    private String getLibs() {
        File folder = new File("libs");
        FilenameFilter jarFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".jar");
            }
        };
        File[] listOfFiles = folder.listFiles(jarFilter);
        List<String> libs = new ArrayList<>();
        libs.add(Paths.get(BuildUtils.ANDROID_HOME, "platforms", (String) configFile.get("targetSDK"), "android.jar").toString());
        for (File file : listOfFiles) {
            libs.add(Paths.get("libs", file.getName()).toString());
        }
        libs.add("obj");
        return String.join(BuildUtils.delimiter, libs);
    }

    /**
     * Method from parent class. Checks for JAVA_HOME, ANDROID_HOME, their related paths. Also checks if
     * it can read the target SDK, build tools version and package name from the configuration file.
     *
     * @param cmd CommandLine value that is used to get the parameter used to execute the command.
     * @returnTrue/False whether the requirements are checked.
     */
    boolean checkRequirements(CommandLine cmd) {
        System.out.println("Checking requirements...");

        if (!BuildUtils.checkJavaEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'JAVA_HOME' variable. Also add the %JAVA_HOME%/bin path to the 'PATH' variable.");
            return false;
        }

        if (!BuildUtils.checkAndroidEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'ANDROID_HOME' variable with " +
                    "the path of the Android SDK and add the path to the Android SDK tools and platform-tools in the 'PATH' variable.");
            return false;
        }

        if (configFile == null) {
            System.err.println("Can't read configuration file. Make sure it is in the root folder and try again.");
            return false;
        } else {
            String targetSDK = (String) configFile.get("targetSDK");
            if (targetSDK == null || targetSDK.equals("")) {
                System.err.println("Can't read the target SDK. Make sure it is in the configuration file and try again.");
                return false;
            }
            String buildToolsVersion = (String) configFile.get("buildToolsVersion");
            if (buildToolsVersion == null || buildToolsVersion.equals("")) {
                System.err.println("Can't read the build tools version. Make sure you set it in the configuration file and try again.");
                return false;
            }
            String packageName = (String) configFile.get("packageName");
            if (packageName == null || packageName.equals("")) {
                System.err.println("Can't read the package. Make sure it is in the configuration file and try again.");
                return false;
            }
        }
        System.out.println("Requirements checked");
        return true;
    }

    /**
     * Method from the parent class. The compile task has no available options.
     */
    void initOptions() {
        options = new Options();
    }
}