import java.nio.file.Paths;
import java.util.Locale;

/**
 * Created by Oja on 16.6.2016.
 */
public class BuildUtils {
    static String extraCommand = setExtraCommand();
    static String delimiter = setDelimiter();
    static String ANDROID_HOME = System.getenv("ANDROID_HOME");
    static String JAVA_HOME = System.getenv("JAVA_HOME");

    static boolean checkAndroidEnvironmentVariables() {
        if (ANDROID_HOME == null) {
            return false;
        }
        String androidTools = Paths.get(ANDROID_HOME, "tools").toString();
        String androidPlatformTools = Paths.get(ANDROID_HOME, "platform-tools").toString();
        String path = System.getenv("PATH");
        if (path == null || !path.contains(androidTools) || !path.contains(androidPlatformTools)) {
            return false;
        }
        return true;
    }

    static boolean checkJavaEnvironmentVariables() {
        if (JAVA_HOME == null) {
            return false;
        }
        String javacPath = Paths.get(JAVA_HOME, "bin").toString();
        String path = System.getenv("PATH");
        if (path == null || !path.contains(javacPath)) {
            return false;
        }
        return true;
    }

    private static String setExtraCommand() {
        if (isWindows()) {
            return "cmd /c ";
        }
        return "";
    }

    private static String setDelimiter() {
        if (isWindows()) {
            return ";";
        }
        return ":";
    }

    private static boolean isWindows() {
        System.out.println("Checking operating system");
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((OS.contains("mac")) || (OS.contains("darwin"))) {
            return false;
        } else if (OS.contains("win")) {
            return true;
        } else if (OS.contains("nux")) {
            return false;
        } else {
            return true;
        }
    }
}