import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Oja on 18.6.2016.
 */
abstract class ProjectTask {
    /**
     * JSON object of the configuration file where information such as project name, package, build options etc. are stored.
     */
    JSONObject configFile;
    /**
     * Options variable of available parameters for a task
     */
    Options options;

    ProjectTask() {
        initOptions();
    }

    /**
     * Abstract method for initializing the options. The options are the available parameters for the task.
     */
    abstract void initOptions();

    /**
     * Abstract method that parses the input parameters and uses them for the task.
     *
     * @param args String array value of the input parameters.
     */
    abstract void parse(String[] args);

    /**
     * Abstract method that checks the requirements needed to execute the task.
     *
     * @param cmd CommandLine value that is used to get the parameter used to execute the command.
     * @return True/False value depending on whether the requirements are met.
     */
    abstract boolean checkRequirements(CommandLine cmd);

    /**
     * Method that prints out the available options of the command.
     *
     * @param description String value with the description of teh task.
     */
    void help(String description) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Uber Build System: " + description, options);
        System.exit(0);
    }

    /**
     * Method that executes the command from the configuration file.
     *
     * @param isBefore A boolean value that is used to get either to before or after command.
     * @param task     A String value representing the type of task (compile, pack, launch).
     */
    void executeConfigFileScript(boolean isBefore, String task) {
        String command;
        if (isBefore) {
            System.out.println("Executing doBefore script for " + task + " task...");
            command = getBeforeCommandFromConfigFile(task);
        } else {
            System.out.println("Executing doAfter script for " + task + " task...");
            command = getAfterCommandFromConfigFile(task);
        }
        if (!command.equals("")) {
            Runtime rt = Runtime.getRuntime();
            try {
                Process proc = rt.exec(BuildUtils.extraCommand + command);
                int exitVal = proc.waitFor();
                if (exitVal == 0) {
                    System.out.println("Script successfully ran");
                } else {
                    System.out.println("Failed to run script");
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                    String response;
                    while ((response = stdInput.readLine()) != null) {
                        System.err.println(response);
                    }
                }
            } catch (IOException | InterruptedException ex) {
                System.out.println("Failed to run script");
                System.err.println("Failed to process command: " + ex);
            }
        }
    }

    /**
     * Method that reads the 'doBefore' command from the JSON configuration file.
     *
     * @param task A String value representing the type of task (compile, pack, launch).
     * @return The appropriate command in a string format.
     */
    private String getBeforeCommandFromConfigFile(String task) {
        if (configFile != null) {
            JSONObject tasksObject = (JSONObject) configFile.get("tasks");
            JSONObject taskObject = (JSONObject) tasksObject.get(task);
            JSONObject beforeObject = (JSONObject) taskObject.get("doBefore");
            return (String) beforeObject.get("task");
        } else {
            return "";
        }
    }

    /**
     * Method that reads the 'doAfter' command from the JSON configuration file.
     *
     * @param task A String value representing the type of task (compile, pack, launch).
     * @return The appropriate command in a string format.
     */
    private String getAfterCommandFromConfigFile(String task) {
        if (configFile != null) {
            JSONObject tasksObject = (JSONObject) configFile.get("tasks");
            JSONObject taskObject = (JSONObject) tasksObject.get(task);
            JSONObject afterObject = (JSONObject) taskObject.get("doAfter");
            return (String) afterObject.get("task");
        } else {
            return "";
        }
    }

    /**
     * Method that reads the configuration file which is a JSON file stored in the root folder.
     *
     * @return A JSONObject value of the configuration file or null if the file can not be read.
     */
    JSONObject readConfigFile() {
        System.out.println("Reading configuration file.");
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("configFile.json"));
            System.out.println("File read.");
            return (JSONObject) obj;
        } catch (IOException | ParseException e) {
            System.out.println("Failed to read configuration file");
            System.err.println(e);
            return null;
        }
    }
}
