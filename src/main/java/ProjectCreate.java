import org.apache.commons.cli.*;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;

/**
 * Created by Oja on 16.6.2016.
 */

class ProjectCreate extends ProjectTask {

    ProjectCreate() {
        super();
    }

    /**
     * Method from the parent class. It parses the input parameters, checks the requirements for this task.
     * If everything is fine then the 'Create project' method gets called. If not the 'Help' method is called
     * and the available options are shown to the user.
     *
     * @param args String array value of the input parameters.
     */
    void parse(String[] args) {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("help")) {
                help("Create an Android project");
            }

            if (checkRequirements(cmd)) {
                String projectName = cmd.getOptionValue("name");
                String projectPackage = cmd.getOptionValue("package");
                String targetID = cmd.getOptionValue("target");

                String activityName = cmd.getOptionValue("activity");
                if (activityName == null) {
                    activityName = "MainActivity";
                }

                String projectPath = cmd.getOptionValue("path");
                if (projectPath == null) {
                    projectPath = projectName;
                } else {
                    projectPath = Paths.get(projectPath, projectName).toString();
                }
                createProject(projectName, projectPath, projectPackage, activityName, targetID);
            } else {
                help("Create an Android project");
            }
        } catch (ParseException ex) {
            System.err.println("Failed to parse command line properties:" + ex);
            help("Create an Android project");
        }
    }

    /**
     * Method that created the Android project using the command line and the Android SDK.
     * If the project is successfully created, a custom configuration file is created and the old
     * Ant ones are deleted.
     *
     * @param projectName    String value of the name of the project.
     * @param projectPath    String value of the location of the project.
     * @param projectPackage String value of the package of the project.
     * @param activityName   String value of the first activity of the project.
     * @param targetID       String value of the target ID for the project
     */
    private void createProject(String projectName, String projectPath, String projectPackage, String activityName, String targetID) {
        System.out.println("Creating an Android project...");
        String command = BuildUtils.extraCommand + "android create project -n " + projectName +
                " -p " + projectPath +
                " -k " + projectPackage +
                " -a " + activityName +
                " -t " + targetID;
        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader output = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line;
            while ((line = output.readLine()) != null) {
                System.out.println(line);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                deleteOldConfigFiles(projectPath);
                createConfigFile(projectPath, targetID, projectPackage, projectName);
                System.out.println("Android project created");
            } else {
                System.out.println("Failed to create and Android project");
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String error;
                while ((error = stdInput.readLine()) != null) {
                    System.err.println(error);
                }
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Failed to create and Android project");
            System.err.println("Failed to process command: " + ex);
        } finally {
            System.exit(0);
        }
    }

    /**
     * Method that deletes old Ant configuration files created with the 'Create' command.
     *
     * @param projectPath String value of the path where the files are located.
     */
    private void deleteOldConfigFiles(String projectPath) {
        System.out.println("Deleting Ant configuration files...");
        Path buildXml = Paths.get(projectPath, "build.xml");
        Path antProperties = Paths.get(projectPath, "ant.properties");
        Path localProperties = Paths.get(projectPath, "local.properties");
        Path projectProperties = Paths.get(projectPath, "project.properties");
        try {
            Files.delete(buildXml);
            Files.delete(antProperties);
            Files.delete(localProperties);
            Files.delete(projectProperties);
        } catch (NoSuchFileException ex) {
            System.err.println("No Such files in directory");
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Method from parent class. Checks for JAVA_HOME, ANDROID_HOME, their related paths, a required name for
     * the project, package for the project and a valid target ID.
     *
     * @param cmd CommandLine value that is used to get the parameter used to execute the command.
     * @returnTrue/False whether the requirements are checked.
     */
    boolean checkRequirements(CommandLine cmd) {
        System.out.println("Checking requirements...");

        if (!BuildUtils.checkJavaEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'JAVA_HOME' variable. Also add the %JAVA_HOME%/bin path to the 'PATH' variable.");
            return false;
        }

        if (!BuildUtils.checkAndroidEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'ANDROID_HOME' variable with " +
                    "the path of the Android SDK and add the path to the Android SDK tools and platform-tools in the 'PATH' variable.");
            return false;
        }

        if (!cmd.hasOption("name")) {
            System.err.println("You need to have a name for the project.");
            return false;
        }
        if (!cmd.hasOption("package")) {
            System.err.println("You need to have a package for the project.");
            return false;
        }
        if (!cmd.hasOption("target")) {
            System.err.println("You need to have a target for the project.");
            return false;
        } else if (!checkTarget(cmd.getOptionValue("target"))) {
            System.err.println("You don't have that SDK version on your computer.");
            return false;
        }
        System.out.println("Requirements checked");
        return true;
    }

    /**
     * Method that checks if the targetID given by the user is installed in the computer.
     *
     * @param targetID String value of the targetID of the SDK.
     * @return True/False value whether that version of the SDK is installed.
     */
    private boolean checkTarget(String targetID) {
        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(BuildUtils.extraCommand + "android list target");
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String response;
            String target = "\"" + targetID + "\"";
            while ((response = stdInput.readLine()) != null) {
                if (response.contains(target)) {
                    return true;
                }
            }
            return false;
        } catch (IOException ex) {
            System.err.println("Failed to process command: " + ex);
            return false;
        }
    }

    /**
     * Method from the parent class. The create task has the following available options.
     * -help (shows help of the available options)
     * -name (name for the project about to be created)
     * -target (targetID for the SDK for the project)
     * -package (name for the package of the project)
     * -activity (name for the first activity that will be in the new project)
     * -path (location where the project will be created)
     */
    void initOptions() {
        options = new Options();
        options.addOption("help", false, "Show help");

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("name");
        OptionBuilder.withDescription("The name for the project");
        Option nameOption = OptionBuilder.create("name");
        options.addOption(nameOption);

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("targetID");
        OptionBuilder.withDescription("Target ID of the system image to use with the new AVD");
        Option targetOption = OptionBuilder.create("target");
        options.addOption(targetOption);

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("namespace");
        OptionBuilder.withDescription("Package namespace");
        Option packageOption = OptionBuilder.create("package");
        options.addOption(packageOption);

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("name");
        OptionBuilder.withDescription("Name for the default Activity class");
        Option activityOption = OptionBuilder.create("activity");
        options.addOption(activityOption);

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("locationPath");
        OptionBuilder.withDescription("Location path of the project");
        Option pathOptions = OptionBuilder.create("path");
        options.addOption(pathOptions);
    }

    /**
     * Method that creates the configuration file later used in the other tasks. The configuration file is a JSON file
     * with information about the project name, package name, target SDK, build tools version and the shell commands
     * that can be used before and after each task.
     *
     * @param path        Path where the file will be created.
     * @param target      Target SDK for the project.
     * @param packageName Name of the project.
     * @param projectName Name of the package of the project.
     */
    private void createConfigFile(String path, String target, String packageName, String projectName) {
        System.out.println("Creating the configuration file...");

        JSONObject doBeforeObject = new JSONObject();
        JSONObject doAfterObject = new JSONObject();
        doBeforeObject.put("task", "");
        doAfterObject.put("task", "");

        JSONObject compileObject = new JSONObject();
        compileObject.put("doBefore", doBeforeObject);
        compileObject.put("doAfter", doAfterObject);
        JSONObject packageObject = new JSONObject();
        packageObject.put("doBefore", doBeforeObject);
        packageObject.put("doAfter", doAfterObject);
        JSONObject launchObject = new JSONObject();
        launchObject.put("doBefore", doBeforeObject);
        launchObject.put("doAfter", doAfterObject);

        JSONObject taskObject = new JSONObject();
        taskObject.put("compile", compileObject);
        taskObject.put("package", packageObject);
        taskObject.put("launch", launchObject);

        JSONObject configObject = new JSONObject();
        configObject.put("tasks", taskObject);
        configObject.put("targetSDK", target);
        configObject.put("packageName", packageName);
        configObject.put("projectName", projectName);
        configObject.put("buildToolsVersion", "");
        try {
            FileWriter writer = new FileWriter(Paths.get(path, "configFile.json").toString());
            writer.write(configObject.toJSONString());
            writer.flush();
            writer.close();
            System.out.println("Configuration file created");
        } catch (IOException ex) {
            System.out.println("Filed to create a configuration file");
            System.err.println(ex);
        }
    }
}