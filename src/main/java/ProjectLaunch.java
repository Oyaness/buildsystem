import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

/**
 * Created by Oja on 17.6.2016.
 */
class ProjectLaunch extends ProjectTask {

    ProjectLaunch() {
        super();
        configFile = readConfigFile();
    }

    /**
     * Method from the parent class. It parses the input parameters, checks the requirements for this task.
     * If everything is fine then the 'Create project' method gets called. If not the 'Help' method is called
     * and the available options are shown to the user.
     *
     * @param args String array value of the input parameters.
     */
    void parse(String[] args) {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("help")) {
                help("Launching an Android project");
            }

            if (checkRequirements(cmd)) {
                if (cmd.hasOption("d")) {
                    if (checkDevice()) {
                        launchProjectOnDevice(cmd.getOptionValue("name"));
                    } else {
                        System.exit(0);
                    }
                } else {
                    launchProjectOnEmulator(cmd.getOptionValue("name"));
                }
            } else {
                help("Launching an Android project");
            }
        } catch (ParseException ex) {
            System.err.println("Failed to parse command line properties:" + ex);
            help("Launching an Android project");
        }
    }

    /**
     * Method used to launch APK on device.
     *
     * @param projectName String value of the name of the APK.
     */
    private void launchProjectOnDevice(String projectName) {
        System.out.println("Launching on device...");
        String apkLocation = Paths.get("bin", projectName).toString();
        String command = BuildUtils.extraCommand + "adb -d install " + apkLocation;
        launchApplication(command);
    }

    /**
     * Method used to launch APK on emulator.
     *
     * @param projectName String value of the name of the APK.
     */
    private void launchProjectOnEmulator(String projectName) {
        System.out.println("Launching on emulator. Make sure your emulator is opened.");
        String apkLocation = Paths.get("bin", projectName).toString();
        String command = BuildUtils.extraCommand + "adb -e install " + apkLocation;
        launchApplication(command);
    }

    /**
     * Method used to launch the application with command line.
     *
     * @param command String value of the appropriate command for launching.
     */
    private void launchApplication(String command) {
        executeConfigFileScript(true, "launch");
        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            int exitVal = process.waitFor();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            if (exitVal == 0) {
                System.out.println("Android project is launched");
                executeConfigFileScript(false, "launch");
            } else {
                System.out.println("Android project failed to launch");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
            }
        } catch (IOException | InterruptedException ex) {
            System.err.println("Failed to process command: " + ex);
        } finally {
            System.exit(0);
        }
    }

    /**
     * Method from parent class. Checks for JAVA_HOME, ANDROID_HOME, their related paths. Also checks if the user has entered
     * valid launch target options and a required APK name.
     *
     * @param cmd CommandLine value that is used to get the parameter used to execute the command.
     * @returnTrue/False whether the requirements are checked.
     */
    boolean checkRequirements(CommandLine cmd) {
        System.out.println("Checking requirements...");

        if (!BuildUtils.checkJavaEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'JAVA_HOME' variable. Also add the %JAVA_HOME%/bin path to the 'PATH' variable.");
            return false;
        }

        if (!BuildUtils.checkAndroidEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'ANDROID_HOME' variable with " +
                    "the path of the Android SDK and add the path to the Android SDK tools in the 'PATH' variable.");
            return false;
        }

        if (!cmd.hasOption("name")) {
            System.err.println("You need to have a name for the project.");
            return false;
        }
        if (!cmd.hasOption("e") && !cmd.hasOption("d")) {
            System.err.println("You have to pick a launch target.");
            return false;
        }
        if (cmd.hasOption("e") && cmd.hasOption("d")) {
            System.err.println("Pick only one launch target.");
            return false;
        }
        System.out.println("Requirements checked");
        return true;
    }

    /**
     * Method used to check if there is a device connected on the computer.
     *
     * @return True/False value whether a device is connected.
     */
    private boolean checkDevice() {
        System.out.println("Checking connected device.");

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(BuildUtils.extraCommand + "adb devices");
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                if (s.contains("device") && !s.contains("devices")) {
                    System.out.println("Device is connected.");
                    return true;
                }
            }
            System.out.println("There is no connected device.");
            return false;
        } catch (IOException ex) {
            System.err.println("Failed to process command: " + ex);
            return false;
        }
    }

    /**
     * Method from the parent class. The create task has the following available options.
     * -help (shows help of the available options)
     * -d (option that allows the user to build on device)
     * -e (option that allows the user to build on emulator)
     * -name (name of the APK file that is supposed to be launched)
     */
    void initOptions() {
        options = new Options();
        options.addOption("help", false, "Show help");
        options.addOption("d", false, "Launch on device");
        options.addOption("e", false, "Launch on emulator");

        OptionBuilder.hasArg();
        OptionBuilder.withArgName("apkFile");
        OptionBuilder.withDescription("The name for the project");
        Option nameOption = OptionBuilder.create("name");
        options.addOption(nameOption);
    }
}