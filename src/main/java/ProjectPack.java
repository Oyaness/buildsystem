import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.Paths;

/**
 * Created by Oja on 17.6.2016.
 */
public class ProjectPack extends ProjectTask {

    public ProjectPack() {
        super();
        configFile = readConfigFile();
    }

    /**
     * Method from the parent class. It parses the input parameters, checks the requirements for this task.
     * If everything is fine then the 'Pack project' method gets called.
     *
     * @param args String array value of the input parameters.
     */
    public void parse(String[] args) {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);

            if (checkRequirements(cmd)) {
                executeConfigFileScript(true, "package");
                packProject();
            } else {
                System.exit(0);
            }
        } catch (ParseException ex) {
            System.err.println("Failed to parse command line properties:" + ex);
            System.exit(0);
        }
    }

    /**
     * Method used to do the pack process.
     */
    private void packProject() {
        createDexFile();
        createAPK();
    }

    /**
     * Method that is used to create a dex file from all the compiled classes.
     */
    private void createDexFile() {
        System.out.println("Creating dex file...");
        Runtime rt = Runtime.getRuntime();
        String dxLocation = Paths.get(BuildUtils.ANDROID_HOME, "build-tools", (String) configFile.get("buildToolsVersion"), "dx").toString();
        String binPath = Paths.get("bin", "classes.dex").toString();
        String command = BuildUtils.extraCommand + dxLocation + " --dex --verbose --output=" + binPath + " obj libs";
        try {
            Process process = rt.exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("Dex file is created");
            } else {
                System.out.println("Failed to create dex file");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Failed to create dex file");
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method that is used to create an unsingned APK file.
     */
    private void createAPK() {
        System.out.println("Creating APK...");
        String aaptLocation = Paths.get(BuildUtils.ANDROID_HOME, "build-tools", (String) configFile.get("buildToolsVersion"), "aapt").toString();
        String androidLocation = Paths.get(BuildUtils.ANDROID_HOME, "platforms", (String) configFile.get("targetSDK"), "android.jar").toString();
        String apkName = configFile.get("projectName") + ".unsigned.apk";
        String apkLocation = Paths.get("bin", apkName).toString();
        String command = BuildUtils.extraCommand + aaptLocation + " package -v -f -M AndroidManifest.xml -S res -I " + androidLocation + " -F " + apkLocation + " bin";

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("APK file is created");
                if (checkKeystore()) {
                    signApk();
                } else {
                    createDebugKeystore();
                }
            } else {
                System.out.println("APK file failed to create");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("APK file failed to create");
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method that is used to create a debug keystore.
     */
    private void createDebugKeystore() {
        System.out.println("Creating a debug keystore");
        String command = BuildUtils.extraCommand + "keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey " +
                "-dname \"CN=UBER, OU=UBER, O=UBER, L=Netherlands, S=state, C=country code\" -keypass android -keyalg RSA -keysize 2048 -validity 10000";

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("Debug keystore created");
                signApk();
            } else {
                System.out.println("Failed to create debug keystore");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Failed to create debug keystore");
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method that is used to sign the unsigned APK with the debug keystore.
     */
    private void signApk() {
        System.out.println("Signing APK...");
        String apkNameUnsigned = Paths.get("bin", configFile.get("projectName") + ".unsigned.apk").toString();
        String apkNameSigned = Paths.get("bin", configFile.get("projectName") + ".signed.apk").toString();
        String command = BuildUtils.extraCommand + "jarsigner -verbose -keystore debug.keystore -storepass android -keypass android -signedjar " +
                apkNameSigned + " " + apkNameUnsigned + " androiddebugkey";

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("APK signed");
                zipAlignApk();
            } else {
                System.out.println("Failed to sign APK");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
                System.exit(0);
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Failed to sign APK");
            System.err.println("Failed to process command: " + ex);
            System.exit(0);
        }
    }

    /**
     * Method that is used to zip align the signed APK.
     */
    private void zipAlignApk() {
        System.out.println("Zip aligning the APK...");
        String apkNameSigned = Paths.get("bin", configFile.get("projectName") + ".signed.apk").toString();
        String apkNameFinal = Paths.get("bin", configFile.get("projectName") + ".apk").toString();
        String zipLocation = Paths.get(BuildUtils.ANDROID_HOME, "build-tools", (String) configFile.get("buildToolsVersion"), "zipalign").toString();
        String command = BuildUtils.extraCommand + zipLocation + " -v -f 4 " + apkNameSigned + " " + apkNameFinal;

        Runtime rt = Runtime.getRuntime();
        try {
            Process process = rt.exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String response;
            while ((response = stdInput.readLine()) != null) {
                System.out.println(response);
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("Zip align done");
                executeConfigFileScript(false, "package");
            } else {
                System.out.println("Zip align failed");
                stdInput = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((response = stdInput.readLine()) != null) {
                    System.err.println(response);
                }
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println("Zip align failed");
            System.err.println("Failed to process command: " + ex);
        } finally {
            System.exit(0);
        }
    }

    /**
     * Method that checks for an existing debug keystore.
     *
     * @return True/False value whether there is a keystore or not.
     */
    private boolean checkKeystore() {
        File folder = new File(".");
        FilenameFilter keyStoreFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".keystore");
            }
        };
        File[] listOfFiles = folder.listFiles(keyStoreFilter);
        for (File file : listOfFiles) {
            if (file.getName().equals("debug")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method from parent class. Checks for JAVA_HOME, ANDROID_HOME, their related paths. Also checks if
     * it can read the target SDK, build tools version and package name from the configuration file.
     *
     * @param cmd CommandLine value that is used to get the parameter used to execute the command.
     * @returnTrue/False whether the requirements are checked.
     */
    boolean checkRequirements(CommandLine cmd) {
        System.out.println("Checking requirements...");
        if (!BuildUtils.checkJavaEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'JAVA_HOME' variable. Also add the %JAVA_HOME%/bin path to the 'PATH' variable.");
            return false;
        }

        if (!BuildUtils.checkAndroidEnvironmentVariables()) {
            System.err.println("Please check your environment variables. Make sure you have added 'ANDROID_HOME' variable with " +
                    "the path of the Android SDK and add the path to the Android SDK tools and platform-tools in the 'PATH' variable.");
            return false;
        }

        if (configFile == null) {
            System.err.println("Can't read configuration file. Make sure it is in the root folder and try again.");
            return false;
        } else {
            String targetSDK = (String) configFile.get("targetSDK");
            if (targetSDK == null || targetSDK.equals("")) {
                System.err.println("Can't read the target SDK. Make sure it is in the configuration file and try again.");
                return false;
            }
            String buildToolsVersion = (String) configFile.get("buildToolsVersion");
            if (buildToolsVersion == null || buildToolsVersion.equals("")) {
                System.err.println("Can't read the build tools version. Make sure it is in the configuration file and try again.");
                return false;
            }
            String projectName = (String) configFile.get("projectName");
            if (projectName == null || projectName.equals("")) {
                System.err.println("Can't read the project name. Make sure it is in the configuration file and try again.");
                return false;
            }
            System.out.println("Requirements checked");
            return true;
        }
    }

    /**
     * Method from the parent class. The compile task has no available options.
     */
    void initOptions() {
        options = new Options();
    }
}
